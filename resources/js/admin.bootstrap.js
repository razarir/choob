/* --> Admin / START
==================================================================================================== */
import {createApp} from 'vue';
import admin_router from "./routes/admin";
import App from './admin/App.vue';
import VueProgressBar from "@aacassandra/vue3-progressbar";

window._ = require('lodash');
import mitt from 'mitt'


// App.components(index);
const app =
  createApp(
    /*  {
      components:{
        App,
        // index
      }
    }*/
    App
  )
    .use(admin_router)
    // a.component('index',index)
    .use(VueProgressBar, {
      color: 'rgb(143, 255, 199)',
      failedColor: 'red',
      height: '15px',
      thickness: '15px',
    })
    .use(require('vue-script2'))
app.config.globalProperties.bus = mitt();
// var openInEditor = require('launch-editor-middleware')
// app.use('/__open-in-phpstorm', openInEditor())
app.mount("#app");

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/*
var a =window.axios.create({}) ;
a.interceptors.request.use(function (config)  {
  // console.log(app)
  // console.log(app.$Progress)
  // console.log(app.$Progress.start)
  // console.log(app.$Progress.start())
  a.$Progress.start(); // for every request start the progress

  return config;
});
a.interceptors.response.use(function (response)  {
  a.$Progress.finish(); // finish when a response is received
  return response;
});

*/

export default app;