@extends('admin.master')
@section('title')
ایجاد محصول جدید
@endsection
@section('content')

 <div class="container-fluid">
  <div class="row">
   <div class="col-md-6 ml-auto mr-auto">
    <div class="rotating-card-container">
     <div class="card card-rotate card-background">
      <div class="front front-background" style="background-image:url('{{asset('assets/admin/img/bg3.jpg')}}');">
       <div class="card-body">
        <h6 class="card-category">Full Background Card</h6>
        <a href="javascript:;">
         <h3 class="card-title">This Background Card Will Rotate on Hover</h3>
        </a>
        <p class="card-description">
         Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
        </p>
       </div>
      </div>

      <div class="back back-background" style="background-image: url('{{asset('assets/admin/img/card-3.jpg')}}');">
       <div class="card-body">
        <h5 class="card-title">
         Manage Post
        </h5>
        <p class="card-description">As an Admin, you have shortcuts to edit, view or delete the posts.</p>
        <div class="footer justify-content-center">
         <a href="javascript:;" class="btn btn-info btn-just-icon btn-fill btn-round">
          <i class="material-icons">subject</i>
         </a>
         <a href="javascript:;" class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
          <i class="material-icons">mode_edit</i>
         </a>
         <a href="javascript:;" class="btn btn-danger btn-just-icon btn-fill btn-round">
          <i class="material-icons">delete</i>
         </a>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
@endsection

