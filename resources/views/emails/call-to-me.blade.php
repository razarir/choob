<!doctype html>
<html lang="ar" dir="rtl">
<head>
 <!-- Required meta tags -->
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.rtl.min.css" integrity="sha384-trxYGD5BY4TyBTvU5H23FalSCYwpLA0vWEvXXGm5eytyztxb+97WzzY+IWDOSbav" crossorigin="anonymous">
 <link rel="stylesheet" href="{{asset('font/persian.css')}}">
 <title>فرم تماس</title>
</head>
<body>
<div class="container py-5 border mt-5 rounded">
 <h1 class="text-center text-primary ">تماس با ما</h1>
 <br>
 <form action="{{route('contact.store')}}" method="post">
  @csrf
  <div class="row">
   <div class="col">
    <div class="input-group mb-3">
     <span class="input-group-text" id="basic-addon1">نام</span>
     <input type="text" class="form-control" placeholder="نام" aria-label="Username" aria-describedby="basic-addon1"  name="name" value="{{$data['name']}}">
    </div>
   </div>
   <div class="col">
    <div class="input-group mb-3">
     <span class="input-group-text" id="basic-addon1">نام خانوادگی</span>
     <input type="text" class="form-control" placeholder="نام خانوادگی" aria-label="Username" aria-describedby="basic-addon1"  name="last_name" value="{{$data['last_name']}}">
    </div>
   </div>
  </div>
  <div class="row">
   <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon1">ایمیل</span>
    <input type="text" class="form-control" placeholder="ایمیل" aria-label="Username" aria-describedby="basic-addon1"  name="email" value="{{$data['email']}}">
   </div>
  </div>
  <div class="row">
   <div class="input-group mb-3">
    <span class="input-group-text" id="basic-addon1">موبایل</span>
    <input type="text" class="form-control" placeholder="موبایل" aria-label="Username" aria-describedby="basic-addon1"
           name="phone" value="{{$data['phone']}}">
   </div>
  </div>
  <div class="row">
   <div class="input-group">
    <span class="input-group-text">پیام</span>
    <textarea class="form-control" aria-label="With textarea" name="content" >{{$data['content']}}</textarea>
   </div>
  </div>
  <div class="row p-5 content-center">
   <div class="col-12  content-center">
{{--    <button class="btn btn-success  btn-lg mr-auto" type="submit">ارسال</button>--}}
   </div>
  </div>
 </form>

</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
-->
</body>
</html>