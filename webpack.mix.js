const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
  require('postcss-import'),
  require('tailwindcss'),
  require('autoprefixer'),
]);
  mix.js('resources/js/admin.bootstrap.js', 'public/js').vue();
// mix.js('resources/js/app.js', 'public/js').vue();

// mix.js('resources/js/app.js', 'public/js')
//   .js('resources/js/haghighi.bootstrap.js', 'public/js')
//   .js('resources/js/hoghoghi.bootstrap.js', 'public/js')
//   .js('resources/js/freelancer.bootstrap.js', 'public/js')
//   .js('resources/js/admin.bootstrap.js', 'public/js')
//   .js('resources/js/web.bootstrap.js', 'public/js')
//   .sass('resources/sass/app.scss', 'public/css');

// resolve: {
//   alias: {
//     vue: 'vue/dist/vue.js'
//   }
// }

mix.browserSync({
  proxy: {target: 'http://choob'},
  // proxy: {target:'http://127.0.0.1:8000'},
  // Here you can disable/enable each feature individually
  ghostMode: {
    clicks: false,
    forms: false,
    scroll: false,
    location: false
  },
  files: [
    // './',
    // './routes/web.php',
    // './app/http/controllers/homecontroller.php',
    // './resources/*/*.{js,vue}',
    // './resources/*/*.{blade.php}',
    './resources/**',
    // './resources/js/components/base_table_user.vue'
    // './resources/js/eblagh/**',
    // './resources/lang/**',
    // './resources/sass/**',
    // './resources/sass/eblagh/**',
    // './resources/views/eblagh/**',
  ]
});

//
// const path = require('path');
//
// module.exports = {
//   output: {
//     filename: 'my-first-webpack.bundle.js',
//   },
//   module: {
//     rules: [{ test: /\.txt$/, use: 'raw-loader' }],
//   },
// };