<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
 use HasFactory;

 protected $table = 'products';
 public $timestamps = true;
 protected $fillable = array('code','name','description','price');
 protected $appends = ['categories_implode','get_price'];

 public function tags()
 {
  return $this->morphToMany(Tag::class, 'tagable')->withPivot('value');
 }

 public function categories()
 {
  return $this->belongsToMany(Category::class);
 }

 public function files()
 {
  return $this->morphMany(File::class, 'fileable');
 }

 /*public function props()
 {
  return $this->belongsToMany(Prop::class)->withPivot('value')->withTimestamps();
 }*/

 public function getCategoriesImplodeAttribute()
 {
  return $this->categories->implode('name',' _ ');
 }

 public function getGetPriceAttribute()
 {
  return $this->price.' تومان';
 }

 public function getSpecialWithDefaultAttribute()
 {
  $file = $this->files->first();
  if ( $file=== null) {
   return (new File)->defaultSpecial(Product::class);
  }else{
   return $file->address;
  }
 }
 public function getFileWithDefaultAttribute()
 {
  return $this->files->count() ? $this->files->map(function ($item) {
   return $item->address;
  }) : [(new File())->defaultSpecial(Product::class)];
 }
}