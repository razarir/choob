<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tagable extends Model 
{

    protected $table = 'tagables';
    public $timestamps = true;
    protected $fillable = array('tag_id', 'tagable_id');

}