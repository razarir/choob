<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prop extends Model 
{
use HasFactory;
    protected $table = 'props';
    public $timestamps = true;
    protected $fillable = array('name');

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('value')->withTimestamps();
    }

}