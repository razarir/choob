<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Mobile\Category\CategoryCollection;
use App\Http\Resources\Mobile\Category\SingleCategoryResource;
use App\Models\Category;

class CategoriesController extends Controller
{
 public function index()
 {
  return new CategoryCollection(Category::with('files')->get()) ;
 }

 public function show($id)
 {
  return new SingleCategoryResource(Category::with('tags')->findOrFail($id));
 }
}