<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Mail;

class ContactController extends Controller
{

 public function index()
 {
  return view('web.contact');
 }

 public function store()
 {

  request()->validate([
   'g-recaptcha-response' => 'required|captcha'
  ]);
  Mail::send(
//  ['text' => 'mail'],
   'emails.call-to-me',
   ['data' => request()->all()],
   function ($message) {
    $message
     ->to('info@amazonchoob.ir', 'ادمین')
     ->subject('تماس با ما');
//    ->from('xyz@gmail.com', 'Virat Gandhi');
   });
  return back();
 }
}