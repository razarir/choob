<?php

namespace App\Http\Resources\ProductResourceAdmin\Product;

use App\Http\Resources\Mobile\Category\CategoryCollection;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Product */
class EditProductResource extends JsonResource
{
 /**
  * @param \Illuminate\Http\Request $request
  * @return array
  */
 public function toArray($request)
 {
  return [
   'id' => $this->id,
   'code' => $this->code,
   'name' => $this->name,
   'price' => $this->price,
   'description' => $this->description,
//   'created_at' => $this->created_at,
//   'updated_at' => $this->updated_at,
   'categories_implode' => $this->categories_implode,
   'file_with_default' => $this->file_with_default,
   'get_price' => $this->get_price,
   'special_with_default' => $this->special_with_default,

   'categories' => collect($this->categories)->pluck('id'),
   'tags' => collect($this->tags)->map(function ($tag) {
    return ['tag'=>$tag->id,'name'=>$tag->name,'val'=>$tag->pivot->value];
   }),
   'files'=>$this->files
  ];
 }
}
