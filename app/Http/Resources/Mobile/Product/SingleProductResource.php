<?php

namespace App\Http\Resources\Mobile\Product;

use App\Models\File;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Product */
class SingleProductResource extends JsonResource
{
 /**
  * @param \Illuminate\Http\Request $request
  * @return array
  */
 public function toArray($request)
 {
  return [
   'id' => $this->id,
   'code' => $this->code,
   'name' => $this->name,
   'price' => $this->price,
   'description' => $this->description,
   'created_at' => $this->created_at,
   'updated_at' => $this->updated_at,
   'categories_implode' => $this->categories_implode,
   'categories' => $this->categories->map(function ($category) {
    return $category['name'];
   }),
   'files' => $this->file_with_default,
   $this->mergeWhen($this->whenLoaded('tags'), [
    'tags' => $this->tags->map(function ($tag) {
     return $tag->name.' : '.$tag->pivot->value;
    })
   ]),

  ];
 }
}
