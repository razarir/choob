<?php

namespace App\Http\Resources\Mobile\Product;

use App\Models\File;
use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

/** @mixin \App\Models\Product */
class ProductResource extends JsonResource
{
 /**
  * @param \Illuminate\Http\Request $request
  * @return array
  */
 public function toArray($request)
 {
  return [
   'id' => $this->id,
   'code' => $this->code,
   'name' => $this->name,
   'price' => $this->price,
   'categories'=> $this->categories->map(function ($category) {
    return $category['name'];
   }),
   'special' => $this->special_with_default
  ];
 }
}
