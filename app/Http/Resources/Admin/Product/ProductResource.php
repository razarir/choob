<?php

namespace App\Http\Resources\Admin\Product;

use App\Models\File;
use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

/** @mixin \App\Models\Product */
class ProductResource extends JsonResource
{
 /**
  * @param \Illuminate\Http\Request $request
  * @return array
  */
 public function toArray($request)
 {
  return [
   'id' => $this->id,
   'code' => $this->code,
   'name' => $this->name,
   'get_price' => $this->get_price,
   'categories_implode' => $this->categories_implode,
   'special' => $this->special_with_default
  ];
 }
}
