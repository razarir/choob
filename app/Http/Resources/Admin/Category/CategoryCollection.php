<?php

namespace App\Http\Resources\Admin\Category;

use Illuminate\Http\Resources\Json\ResourceCollection;

/** @see \App\Models\Category */
class CategoryCollection extends ResourceCollection
{
 /**
  * @param \Illuminate\Http\Request $request
  * @return array
  */
 public function toArray($request)
 {
  return [
   'data' => $this->collection,
  ];
 }
}
