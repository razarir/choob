<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
 public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
 {
  parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
  (new \App\Lib\Request())->ready_request();
 }

 public function rules()
 {
  return [
   'name' => 'required|string',
   'price' => 'required|numeric',
   'description' => 'required|string',
   'code' => 'required|string|unique:products',
   'selected_tags' => 'required|array',
   'categories' => 'required|array',
   'files' => [
    'array'
   ],
   'files_info' => [
    'array',
    function ($attribute, $value, $fail) {
     $check = false;
     foreach ($value as $item) {
      if ($item->special) {
       $check = true;
      }
     }
     if (!$check) {
      $fail('باید یکی از فایل‌ها به عنوان شاخص مشخض شده باشد. ');
     }
    },
   ],

  ];
 }

 public function authorize()
 {
  return true;
 }
}