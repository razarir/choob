<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class CallToMe extends Mailable
{
 public function __construct()
 {
  //
 }

 public function build()
 {
  return $this->view('emails.call-to-me');
 }
}