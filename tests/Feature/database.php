<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use App\Models\Prop;
use App\Models\Tag;
use App\Models\File;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;


class database extends TestCase
{
 use DatabaseTransactions;

// protected $seed = true;

 public function testBasic()
 {
  $categories = Category::factory()
   ->has(
    Product::factory()
     ->hasAttached(
      Prop::factory()->count(1),
      ['value' => 'سفید']
     )
     ->hasTags(1)
     ->hasFiles(1)
     ->count(1)
   )
   ->count(1)
   ->create();

  $this->assertTrue(
   $categories->count()===1 &&
   $categories[0]->products->count()===1 &&
   $categories[0]->products[0]->props->count()===1 &&
   $categories[0]->products[0]->tags->count()===1 &&
   $categories[0]->products[0]->files->count()===1
  );
 }


 public function testCreateProp()
 {
  $product = Product::factory()->create();
  $product->props()->attach([
   Prop::factory()->create()->id => ['value' => 'srsddsgdf۱۱۱'],
   Prop::factory()->create()->id => ['value' => '۲۳۴۲۳'],
  ]);
 $product->load('props', 'files', 'categories', 'tags');
 $this->assertTrue($product->props->count()===2);
 }
}