<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\Prop;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
 /**
  * Seed the application's database.
  *
  * @return void
  */
 public function run()
 {

  User::firstOrCreate([
   'name' => 'a@a.a',
   'email' => 'a@a.a'
  ], [
   'password' => \Hash::make('a@a.a'),
  ]);
//         \App\Models\User::factory(10)->create();
//     Model::unguard();
//
//     $this->call(\CategoryTableSeeder::class);
//     $this->command->info('Category table seeded!');
//
//     $this->call('ProductTableSeeder');
//     $this->command->info('Product table seeded!');
//
//     $this->call('TagTableSeeder');
//     $this->command->info('Tag table seeded!');
//
//     $this->call('FileTableSeeder');
//     $this->command->info('File table seeded!');
//     Product::factory(1);
  $categories = Category::factory()
   ->has(
    Product::factory()
     ->hasAttached(
      Tag::factory()->count(2),
      ['value' => 'aaadwd']
     )
     ->hasFiles(2)
     ->count(2)
   )
   ->hasAttached(
    Tag::factory()->count(2),
    ['value' => 'aaadwd']
   )
   ->count(2)
   ->create();
 }
}
