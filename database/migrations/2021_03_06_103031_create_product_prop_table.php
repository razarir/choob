<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductPropTable extends Migration {

	public function up()
	{
		Schema::create('product_prop', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->integer('prop_id')->unsigned();
			$table->string('value', 255);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('product_prop');
	}
}