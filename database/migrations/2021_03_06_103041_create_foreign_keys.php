<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('categories', function(Blueprint $table) {
			$table->foreign('parent')->references('id')->on('categories')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('tagables', function(Blueprint $table) {
			$table->foreign('tag_id')->references('id')->on('tags')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('category_product', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('category_product', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('categories')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_prop', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_prop', function(Blueprint $table) {
			$table->foreign('prop_id')->references('id')->on('props')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('categories', function(Blueprint $table) {
			$table->dropForeign('categories_parent_foreign');
		});
		Schema::table('tagables', function(Blueprint $table) {
			$table->dropForeign('tagables_tag_id_foreign');
		});
		Schema::table('category_product', function(Blueprint $table) {
			$table->dropForeign('category_product_product_id_foreign');
		});
		Schema::table('category_product', function(Blueprint $table) {
			$table->dropForeign('category_product_category_id_foreign');
		});
		Schema::table('product_prop', function(Blueprint $table) {
			$table->dropForeign('product_prop_product_id_foreign');
		});
		Schema::table('product_prop', function(Blueprint $table) {
			$table->dropForeign('product_prop_prop_id_foreign');
		});
	}
}