<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilesTable extends Migration {

	public function up()
	{
		Schema::create('files', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255)->nullable();
			$table->integer('fileable_id')->unsigned();
			$table->string('fileable_type', 255);
			$table->string('address', 255);
			$table->string('mime_type', 255)->nullable();
			$table->string('size', 255)->nullable();
			$table->string('thumbnail', 255)->nullable();
			$table->string('extension', 255)->nullable();
			$table->tinyInteger('special')->nullable();
			$table->string('alt', 255)->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('files');
	}
}