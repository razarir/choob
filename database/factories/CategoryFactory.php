<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class CategoryFactory extends Factory
{
 protected $model = Category::class;

 /**
  * Define the model's default state.
  *
  * @return array
  */
 public function definition()
 {
  $category = Category::all('id')->shuffle()->first();

  $category !== Null ?$category_id = $category->id :$category_id =1;
  return [
   'created_at' => Carbon::now(),
   'updated_at' => Carbon::now(),
   'name' => $this->faker->name,
   'description' => $this->faker->text,
   'code' => $this->faker->word,
   'parent' => $category_id,
  ];
 }

}
