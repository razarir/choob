<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Storage;

class FileFactory extends Factory
{
 protected $model = File::class;

 /**
  * Define the model's default state.
  *
  * @return array
  */
 public function definition()
 {
  return [
   'created_at' => Carbon::now(),
   'updated_at' => Carbon::now(),
   'name' => $this->faker->name,
   'address' => $this->faker->word,
   'mime_type' => $this->faker->mimeType,
   'size' => $this->faker->word,
   'thumbnail' => $this->faker->word,
   'extension' => $this->faker->fileExtension,
   'Special' => $this->faker->randomElement([0,1]),
   'fileable_id' => $this->faker->randomNumber(),
   'fileable_type' => $this->faker->word,
  ];
 }
}
